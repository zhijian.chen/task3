//
//  QQTabBarController.h
//  bbb
//
//  Created by 陈志坚 on 2021/9/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QQTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
