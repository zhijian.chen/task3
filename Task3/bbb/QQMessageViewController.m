//
//  QQMessageViewController.m
//  bbb
//
//  Created by 陈志坚 on 2021/9/1.
//

#import "QQMessageViewController.h"
#import "QQMessageTableViewCell.h"
#import "QQMessageSearchTableViewCell.h"
#import "PersonalCenterViewController.h"
#import "ChatViewController.h"
#import "Masonry.h"

@interface QQMessageViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) NSMutableArray *dataArray;
@property(nonatomic, strong) UITableView *qqMessageTableView;

@end

@implementation QQMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray *arr1 = [NSArray arrayWithObjects:@"tower.jpeg", @"qq群1", @"英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年", @"下午3:00", nil];
    NSArray *arr2 = [NSArray arrayWithObjects:@"tower.jpeg", @"qq群2", @"英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年", @"下午3:00", nil];
    NSArray *arr3 = [NSArray arrayWithObjects:@"tower.jpeg", @"qq群3", @"英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年英雄少年", @"下午3:00", nil];
    self.dataArray = [NSMutableArray arrayWithCapacity:20];
    [self.dataArray addObject:arr1];
    [self.dataArray addObject:arr2];
    [self.dataArray addObject:arr3];
    [self.dataArray addObject:arr3];
    [self.dataArray addObject:arr3];
    [self.dataArray addObject:arr3];
    [self.dataArray addObject:arr3];
    [self.dataArray addObject:arr3];
    [self.dataArray addObject:arr3];
    [self.dataArray addObject:arr3];
    [self.dataArray addObject:arr3];
    
    self.qqMessageTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.qqMessageTableView.delegate = self;
    self.qqMessageTableView.dataSource = self;
    
    self.qqMessageTableView.estimatedRowHeight = 60;
    self.qqMessageTableView.rowHeight = UITableViewAutomaticDimension;
    self.qqMessageTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    [self.qqMessageTableView registerClass:[QQMessageTableViewCell class] forCellReuseIdentifier:NSStringFromClass([QQMessageTableViewCell class])];
    [self.qqMessageTableView registerClass:[QQMessageSearchTableViewCell class] forCellReuseIdentifier:NSStringFromClass([QQMessageSearchTableViewCell class])];
    [self.view addSubview:self.qqMessageTableView];
    [self.qqMessageTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, 0, 0));
        make.width.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:20.0/355 green:221.0/355 blue:255.0/255 alpha:0.8]];
    
    CGFloat barWidth = self.navigationController.navigationBar.bounds.size.width;
    CGFloat barHeight = self.navigationController.navigationBar.bounds.size.height;
    
    
    self.navigationItem.title = @"";
    UIView *leftBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, self.navigationController.navigationBar.bounds.size.height)];
    leftBtnView.backgroundColor = UIColor.clearColor;
    leftBtnView.userInteractionEnabled = YES;
    UIImage *image = [UIImage imageNamed:@"tower.jpeg"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.contentMode = UIViewContentModeScaleToFill;
    btn.imageView.layer.cornerRadius = 15;
    btn.imageView.layer.masksToBounds = YES;
    [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:image forState:UIControlStateNormal];
    [leftBtnView addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@30);
        make.width.equalTo(@30);
        make.left.equalTo(leftBtnView.mas_left);
        make.top.equalTo(leftBtnView.mas_top);
        make.right.equalTo(leftBtnView.mas_right);
        make.bottom.equalTo(leftBtnView.mas_bottom);
//        make.left.equalTo(@-5);
//        make.top.equalTo(@-12);
    }];
    
    
    UILabel *headNameLabel = [[UILabel alloc] init];
    headNameLabel.text = @"Try";
    headNameLabel.font = [UIFont systemFontOfSize:15];
    headNameLabel.textColor = UIColor.whiteColor;
    [leftBtnView addSubview:headNameLabel];
    [headNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btn.mas_right).offset(5);
        make.top.equalTo(btn);
    }];
    
    UIView *circleView = [[UIView alloc] init];
    circleView.backgroundColor = [UIColor colorWithRed:131.0/255 green:230.0/255 blue:180.0/255 alpha:1];
    circleView.layer.cornerRadius = 4;
    circleView.layer.masksToBounds = YES;
    [leftBtnView addSubview:circleView];
    [circleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headNameLabel);
        make.bottom.equalTo(btn);
        make.height.equalTo(@8);
        make.width.equalTo(@8);
    }];
    
    UILabel *statusLabel = [[UILabel alloc] init];
    statusLabel.text = @"在线 >";
    statusLabel.textColor = UIColor.whiteColor;
    statusLabel.font = [UIFont systemFontOfSize:9];
    statusLabel.textAlignment = NSTextAlignmentCenter;
    [leftBtnView addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(circleView);
        make.left.equalTo(circleView.mas_right).offset(3);
        make.bottom.equalTo(btn);
    }];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtnView];
    
    
    UIView *rightBtnView = [[UIView alloc] initWithFrame:CGRectMake(barWidth - 100, 0, 100, barHeight)];
    
    rightBtnView.backgroundColor = UIColor.redColor;
    
    UIButton *addBtn = [[UIButton alloc] init];
    [addBtn setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
    [rightBtnView addSubview:addBtn];
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@25);
        make.width.equalTo(@25);
        make.right.equalTo(@0);
        make.top.equalTo(@-10);
    }];
    
    UIButton *cameraBtn = [[UIButton alloc] init];
    [cameraBtn setImage:[UIImage imageNamed:@"camera.png"] forState:UIControlStateNormal];
    [cameraBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [rightBtnView addSubview:cameraBtn];
    [cameraBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@25);
        make.width.equalTo(@25);
        make.right.equalTo(addBtn.mas_left).offset(-10);
        make.centerY.equalTo(addBtn);
    }];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtnView];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self.navigationController.navigationBar addSubview:btn];
//
//    });
    
//    [self.navigationController.navigationBar addSubview:btn];

    
    
}

-(IBAction)btnClick:(UIButton*)btn{
    NSLog(@"12345");
    PersonalCenterViewController *personalCenterVC = [[PersonalCenterViewController alloc] init];
    personalCenterVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:personalCenterVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 1;
            break;
            
        case 1:
            return self.dataArray.count;
            break;
            
        default:
            return 0;
            break;;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            QQMessageSearchTableViewCell *searchCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([QQMessageSearchTableViewCell class]) forIndexPath:indexPath];
            return searchCell;
            break;
        }
        
        case 1:{
            QQMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([QQMessageTableViewCell class]) forIndexPath:indexPath];
            
            [cell.headerImageView setImage:[UIImage imageNamed:self.dataArray[indexPath.row][0]]];
            cell.nameLabel.text = self.dataArray[indexPath.row][1];
            cell.detailLabel.text = self.dataArray[indexPath.row][2];
            cell.timeLabel.text = self.dataArray[indexPath.row][3];
            return cell;
        }
        default:
            return nil;
            break;
    }

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        ChatViewController *charVC = [[ChatViewController alloc] init];
        charVC.hidesBottomBarWhenPushed = YES;
        charVC.nameStr = self.dataArray[indexPath.row][1];
        charVC.headImageStr = self.dataArray[indexPath.row][0];
        [self.navigationController pushViewController:charVC animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
