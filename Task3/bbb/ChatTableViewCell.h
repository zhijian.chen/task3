//
//  ChatTableViewCell.h
//  bbb
//
//  Created by 陈志坚 on 2021/9/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatTableViewCell : UITableViewCell

@property(nonatomic, strong) UIImageView *headImageView;
@property(nonatomic, strong) UILabel *messageLabel;
@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UIView *bubbleView;

@end

NS_ASSUME_NONNULL_END
