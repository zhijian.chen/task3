//
//  QQMessageSearchTableViewCell.m
//  bbb
//
//  Created by 陈志坚 on 2021/9/2.
//

#import "QQMessageSearchTableViewCell.h"
#import "Masonry.h"

@implementation QQMessageSearchTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.backgroundColor = UIColor.clearColor;
        
        self.searchTextField = [[UITextField alloc] init];
//        self.searchTextField.borderStyle = UITextBorderStyleRoundedRect;
        self.searchTextField.layer.cornerRadius = 15;
        self.searchTextField.placeholder = @" 🔍 搜索";
        self.searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.searchTextField.backgroundColor = [UIColor colorWithRed:245.0/255 green:246.0/255 blue:250.0/255 alpha:1];
        [self.contentView addSubview:self.searchTextField];
        [self.searchTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@15);
            make.right.equalTo(@-15);
            make.height.equalTo(@30);
            make.top.equalTo(@10);
            make.bottom.equalTo(@-10);
        }];
        
    }
    
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
