//
//  ChatViewController.m
//  bbb
//
//  Created by 陈志坚 on 2021/9/2.
//

#import "ChatViewController.h"
#import "ChatTableViewCell.h"
#import "Masonry.h"

@interface ChatViewController ()<UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property(nonatomic, strong) UITextView *messageTextView;
@property(nonatomic, strong) UIView *sendMessageView;
@property(nonatomic, strong) UITableView *messageTableView;
@property(nonatomic, strong) UIView *containerView;
@property CGFloat textViewNowHeight;
@property CGFloat backViewNowHeight;
@property CGFloat textHeight;

@property(nonatomic, strong) NSMutableArray *messageArr;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.containerView = [[UIView alloc] init];
    self.containerView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.containerView];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(self.view);
        make.width.equalTo(self.view);
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.view.mas_top).offset(0);
    }];
    
    NSArray *arr1 = @[@"123", @"4356u543o5u4ufldkngvjiehfiuewhfweiufiuherifuewhriuowehhjnewoirjrenwiorhneiorhnwdfnvds", @"但是健康黑发上看到的上岛咖啡难道是咖啡那位哦日么我惹我"];
    self.messageArr = [NSMutableArray arrayWithArray:arr1];
    
    self.title = self.nameStr;
    
    self.sendMessageView = [[UIView alloc] init];
    self.sendMessageView.backgroundColor = [UIColor colorWithRed:241.0/255 green:241.0/255 blue:250.0/255 alpha:1];
    [self.containerView addSubview:self.sendMessageView];
    [self.sendMessageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.equalTo(@50);
        make.height.greaterThanOrEqualTo(@50);
        make.height.lessThanOrEqualTo(@150);
        make.width.equalTo(self.view);
        make.left.equalTo(self.view.mas_left);
    }];

    //计算一行字的高度
    NSDictionary *dict=@{NSFontAttributeName:[UIFont systemFontOfSize:14.0]};
    CGSize contentSize=[@"我" sizeWithAttributes:dict];
    self.textHeight = contentSize.height;
    NSLog(@"123%f", self.textHeight);
    
    self.messageTextView = [[UITextView alloc] init];
    self.messageTextView.textColor = UIColor.blackColor;
    self.messageTextView.backgroundColor = UIColor.whiteColor;
    self.messageTextView.layer.cornerRadius = 10;
    self.messageTextView.textAlignment = NSTextAlignmentLeft;
    self.messageTextView.delegate = self;
    self.messageTextView.font = [UIFont systemFontOfSize:14];
    self.messageTextView.returnKeyType = UIReturnKeySend;
    self.messageTextView.layoutManager.allowsNonContiguousLayout = NO;
    self.messageTextView.scrollEnabled = NO;
    self.messageTextView.enablesReturnKeyAutomatically = YES;
    
    [self.sendMessageView addSubview:self.messageTextView];
    [self.messageTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
        make.bottom.equalTo(@-8);
        make.height.greaterThanOrEqualTo(@30);
        make.height.lessThanOrEqualTo(@120);
    }];
    
    
    self.messageTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.messageTableView.estimatedRowHeight = 50;
    self.messageTableView.rowHeight = UITableViewAutomaticDimension;
    self.messageTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    self.messageTableView.dataSource = self;
    self.messageTableView.delegate = self;
    [self.messageTableView registerClass:[ChatTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ChatTableViewCell class])];
    
    [self.containerView addSubview:self.messageTableView];
    [self.messageTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.sendMessageView.mas_top);
        make.top.equalTo(self.containerView.mas_top);
        make.width.equalTo(self.containerView);
        make.left.equalTo(self.containerView.mas_left);
    }];
    
    //让约束立即生效
    [self.view layoutIfNeeded];
    self.textViewNowHeight = self.messageTextView.contentSize.height;
    self.backViewNowHeight = self.sendMessageView.frame.size.height;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisAppear:) name:UIKeyboardWillHideNotification object:nil];

    // Do any additional setup after loading the view.
}



- (void)textViewDidChange:(UITextView *)textView{
    if(textView.text.length > 300)
        textView.text = [textView.text substringToIndex:300];
    CGFloat height = self.messageTextView.contentSize.height;
    if(height > self.textViewNowHeight){
        [self.sendMessageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(self.backViewNowHeight + self.textHeight));
        }];

        [self.view layoutIfNeeded];
        self.backViewNowHeight = self.backViewNowHeight + self.textHeight;
        self.textViewNowHeight = height;
    }
    else if(height < self.textViewNowHeight){
        [self.sendMessageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(self.backViewNowHeight - self.textHeight));
        }];

        [self.view layoutIfNeeded];
        self.textViewNowHeight = height;
        self.backViewNowHeight = self.backViewNowHeight - self.textHeight;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messageArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ChatTableViewCell class]) forIndexPath:indexPath];
    [cell.headImageView setImage:[UIImage imageNamed:self.headImageStr]];
    cell.messageLabel.text = self.messageArr[indexPath.row];

    return cell;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        NSLog(@"123");
        [self.messageArr addObject:self.messageTextView.text];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(self.messageArr.count - 1) inSection:0];
        [self.messageTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
        [self.messageTableView reloadData];
        [self.messageTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        self.messageTextView.text = @"";
        [self.messageTextView endEditing:YES];
        [self.sendMessageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@50);
        }];
        [self.view layoutIfNeeded];
        self.textViewNowHeight = self.messageTextView.contentSize.height;
        self.backViewNowHeight = self.sendMessageView.frame.size.height;
        return NO;
    }
    return YES;
}


-(void)keyBoardWillShow:(NSNotification*)sender{
    
    CGRect keyboardFrame = [sender.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardY = keyboardFrame.origin.y;
        //视图整体上升
    [UIView animateWithDuration:0.5 animations:^{self.containerView.transform = CGAffineTransformMakeTranslation(0, keyboardY - self.containerView.frame.size.height);}];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.messageTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)keyboardWillDisAppear:(NSNotification *)sender{
    [UIView animateWithDuration:1 animations:^{self.containerView.transform = CGAffineTransformMakeTranslation(0, 0);}];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
