//
//  AppDelegate.h
//  bbb
//
//  Created by 陈志坚 on 2021/8/31.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(nonatomic, strong) UIWindow *window;

@end

