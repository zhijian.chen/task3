//
//  ChatViewController.h
//  bbb
//
//  Created by 陈志坚 on 2021/9/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatViewController : UIViewController

@property(nonatomic, strong) NSString *nameStr;
@property(nonatomic, strong) NSString *headImageStr;

@end

NS_ASSUME_NONNULL_END
