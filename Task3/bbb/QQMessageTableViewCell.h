//
//  QQMessageTableViewCell.h
//  bbb
//
//  Created by 陈志坚 on 2021/9/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QQMessageTableViewCell : UITableViewCell

@property(nonatomic, strong) UIImageView *headerImageView;
@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UILabel *detailLabel;
@property(nonatomic, strong) UILabel *timeLabel;

@end

NS_ASSUME_NONNULL_END
