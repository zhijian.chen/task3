//
//  ChatTableViewCell.m
//  bbb
//
//  Created by 陈志坚 on 2021/9/2.
//

#import "ChatTableViewCell.h"
#import "Masonry.h"

@implementation ChatTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.backgroundColor = UIColor.clearColor;
        
        self.headImageView = [[UIImageView alloc] init];
        self.headImageView.layer.cornerRadius = 45/2;
        self.headImageView.layer.masksToBounds = YES;
//        self.headImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:self.headImageView];
        [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right).offset(-10);
            make.top.equalTo(self.contentView.mas_top).offset(10);
            make.height.equalTo(@45);
            make.width.equalTo(@45);
//            make.bottom.equalTo(self.contentView.mas_bottom);
        }];
        
        
        self.messageLabel = [[UILabel alloc] init];
        self.messageLabel.font = [UIFont systemFontOfSize:12];
//        self.messageLabel.textColor = UIColor.whiteColor;
        self.messageLabel.textColor = UIColor.whiteColor;
        self.messageLabel.textAlignment = NSTextAlignmentLeft;
        self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.messageLabel.font = [UIFont systemFontOfSize:15];
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.backgroundColor = [UIColor colorWithRed:108.0/255 green:184.0/255 blue:255.0/255 alpha:1];
        self.messageLabel.layer.cornerRadius = 4;
        self.messageLabel.layer.masksToBounds = YES;
        
        [self.contentView addSubview:self.messageLabel];
        [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.headImageView.mas_left).offset(-20);
            make.left.greaterThanOrEqualTo(self.contentView.mas_left).offset(50);
            make.top.equalTo(self.headImageView.mas_top).offset(5);
            make.bottom.lessThanOrEqualTo(@-30);
        }];

    }
    
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
