//
//  QQTabBarController.m
//  bbb
//
//  Created by 陈志坚 on 2021/9/1.
//

#import "QQTabBarController.h"

#import "QQMessageViewController.h"

@interface QQTabBarController ()

- (UIImage*) customImageSize:(UIImage *)image scaleToSize:(CGSize)size;

@end

@implementation QQTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // 添加子控制器
    QQMessageViewController * VC01 = [[QQMessageViewController alloc]init];
    VC01.title = @"消息";
    VC01.view.backgroundColor = [UIColor yellowColor];
    UIImage *image1 = [UIImage imageNamed:@"message.jpeg"];
    image1 = [self customImageSize:image1 scaleToSize:CGSizeMake(30, 30)];
    VC01.tabBarItem.image = image1;
    UINavigationController *qqMessageNav = [[UINavigationController alloc] initWithRootViewController:VC01];
    [self addChildViewController:qqMessageNav];
    
    UIViewController * VC02 = [[UIViewController alloc]init];
    VC02.title = @"联系人";
    VC02.view.backgroundColor = [UIColor redColor];
    UIImage *image2 = [UIImage imageNamed:@"people.png"];
    image2 = [self customImageSize:image2 scaleToSize:CGSizeMake(30, 30)];
    VC02.tabBarItem.image = image2;
    UINavigationController *qqPeopleNav = [[UINavigationController alloc] initWithRootViewController:VC02];
    [self addChildViewController:qqPeopleNav];
    
    UIViewController * VC03 = [[UIViewController alloc]init];
    VC03.title = @"看点";
    UIImage *image3 = [UIImage imageNamed:@"view.png"];
    image3 = [self customImageSize:image3 scaleToSize:CGSizeMake(30, 30)];
    VC03.tabBarItem.image = image3;
    VC03.view.backgroundColor = [UIColor blueColor];
    UINavigationController *qqViewsNav = [[UINavigationController alloc] initWithRootViewController:VC03];
    [self addChildViewController:qqViewsNav];
    
    UIViewController * VC04 = [[UIViewController alloc]init];
    VC04.title = @"动态";
    UIImage *image4 = [UIImage imageNamed:@"dynamic.png"];
    image4 = [self customImageSize:image4 scaleToSize:CGSizeMake(30, 30)];
    VC04.tabBarItem.image = image4;
    UINavigationController *qqDynamicNav = [[UINavigationController alloc] initWithRootViewController:VC04];
    VC04.view.backgroundColor = [UIColor greenColor];
    [self addChildViewController:qqDynamicNav];
}

- (UIImage*) customImageSize:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    scaledImage = [scaledImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return scaledImage;   //返回的就是已经改变的图片
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
