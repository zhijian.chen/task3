//
//  QQMessageSearchTableViewCell.h
//  bbb
//
//  Created by 陈志坚 on 2021/9/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QQMessageSearchTableViewCell : UITableViewCell

@property(nonatomic, strong) UITextField *searchTextField;

@end

NS_ASSUME_NONNULL_END
